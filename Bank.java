class Bank {

  private BankAccount accounts[];
  static int MAX_ACCOUNTS = 15;
  private String name;
  private int no_of_accounts;

  public Bank(String name) {
    this.name = name;
    no_of_accounts = 0;
    accounts = new BankAccount[MAX_ACCOUNTS];
  }

  public int createAccount() {
    if (no_of_accounts >= MAX_ACCOUNTS) return 0;

    accounts[no_of_accounts++] = new BankAccount(no_of_accounts);
    return 1;
  }

  public int createAccount(String email) {
    if (no_of_accounts >= MAX_ACCOUNTS) return 0;

    accounts[no_of_accounts++] = new BankAccount(no_of_accounts, email);
    return 1;
  }

  public int createAccount(double balance) {
    if (no_of_accounts >= MAX_ACCOUNTS) return 0;

    accounts[no_of_accounts++] = new BankAccount(no_of_accounts, balance);
    return 1;
  }

  public int createAccount(String email, double balance) {
    if (no_of_accounts >= MAX_ACCOUNTS) return 0;

    accounts[no_of_accounts++] = new BankAccount(no_of_accounts, email, balance);
    return 1;
  }

  public int getNoAccounts() {
    return no_of_accounts;
  }

  public int transaction(BankAccount b1, BankAccount b2, double amount) {
    if (amount <= 0) return -1;

    if (b1.withdraw(amount) == 0) return 0;

    b2.deposit(amount);
    return 1;
  }
}
