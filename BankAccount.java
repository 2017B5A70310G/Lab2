class BankAccount {

  private int acc_no;
  private double balance;
  private String firstName;
  private String lastName;
  static int MAX_TRANSACTIONS_DAY = 5;
  static double MIN_BALANCE = 1000.0;
  private int num_transactions;

  public BankAccount(int acc_no, String email, double balance) {
    this.acc_no = acc_no;
    this.balance = MIN_BALANCE + balance;

    firstName = email.substring(0, email.indexOf('.'));
    firstName = Character.toUpperCase(firstName.charAt(0)) + firstName.substring(1);

    lastName = email.substring(email.indexOf('.') + 1, email.indexOf('@'));
    lastName = Character.toUpperCase(lastName.charAt(0)) + lastName.substring(1);

    num_transactions = 0;
  }

  public BankAccount(int acc_no, String email) {
    this(acc_no, email, 0);
  }

  public BankAccount(int acc_no, double balance) {
    this(acc_no, "john.doe@gmail.com", balance);

    lastName = lastName + acc_no;
  }

  public BankAccount(int acc_no) {
    this(acc_no, "john.doe@gmail.com", 0);

    lastName = lastName + acc_no;
  }

  public int getAccountNumber() {
    return acc_no;
  }

  public String getName() {
    return firstName + " " + lastName;
  }

  public double getBalance() {
    return balance;
  }

  public int deposit(double amount) {
    if (amount <= 0) return 0;

    balance += (num_transactions < MAX_TRANSACTIONS_DAY) ? amount : 0.95*amount;
    num_transactions++;

    return 1;
  }

  public int withdraw(double amount) {
    if (amount <= 0) return -1;

    if (num_transactions < MAX_TRANSACTIONS_DAY) {
      if (balance - amount < MIN_BALANCE) return 0;

      balance -= amount;
    }
    else {
      if (balance - 1.05*amount < MIN_BALANCE) return 0;

      balance -= 1.05*amount;
    }

    num_transactions++;
    return 1;
  }

  public void printDetails() {
    System.out.println("Acc No : " + getAccountNumber());
    System.out.println("Name : " + getName());
    System.out.println("Balance : " + getBalance());
  }
}
